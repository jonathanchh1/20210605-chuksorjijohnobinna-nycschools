package com.chuksorji.nycschool.network

import okhttp3.Interceptor

interface ConnectivityInterceptor : Interceptor