package com.chuksorji.nycschool.network

import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School
import retrofit2.Response

//Network data retriever.
class NetworkFinder constructor(private var services: NetworkServices) {
    suspend fun getRetrievedSchools(): Response<MutableList<School>> = services.getSchoolData()
    suspend fun getRetrievedSat(): Response<MutableList<SatDetail>> = services.getSatsDetail()

}