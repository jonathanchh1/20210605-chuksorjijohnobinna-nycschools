package com.chuksorji.nycschool.network

import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School
import retrofit2.Response
import retrofit2.http.GET


//All endpoints to be called.
interface NetworkServices{
    @GET("s3k6-pzi2.json")
    suspend fun getSchoolData(): Response<MutableList<School>>
    @GET("f9bf-2cp4.json")
    suspend fun getSatsDetail(): Response<MutableList<SatDetail>>
}
