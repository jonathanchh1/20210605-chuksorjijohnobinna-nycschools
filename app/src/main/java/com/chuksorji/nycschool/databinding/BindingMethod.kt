package com.chuksorji.nycschool.databinding

import android.content.Context
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.chuksorji.nycschool.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import timber.log.Timber

object BindingMethod {
    //This observable is being used for showing the loading progressbar if the image isn't ready to be shown.
   private val isLoading = ObservableField(false)
    @BindingAdapter("compatSrc")
    @JvmStatic
    fun loadImage(view: ImageView, url: String?) {
        //if we don't have url then we'll show a place-holder.
        if (url?.isEmpty() == true) {
            view.setImageDrawable(
                ContextCompat.getDrawable(
                    view.context,
                    R.drawable.ic_place_holder
                )
            )
        } else {
            view.context.imageLoader(view, url)
        }
    }

    //This extension function helps us handle case of either loading the image or showing a loader.
    fun Context.imageLoader(view: ImageView, url: String?) {
        val circularProgressDrawable = CircularProgressDrawable(this)
        circularProgressDrawable.apply {
            strokeWidth = 5f
            centerRadius = 30f
            setColorSchemeColors(
                getColor(R.color.white)
            )
            start()
        }

        Picasso.get()
            .load(url)
            .fit()
            .centerInside()
            .placeholder(circularProgressDrawable)
            .into(view, object : Callback {
                override fun onSuccess() {
                    Timber.d("image was retrieved")
                    isLoading.set(true)

                }

                override fun onError(e: Exception?) {
                    isLoading.set(false)
                    Timber.d("there's an issue while trying to download the image ${e?.message}")
                }
            })
    }



}