package com.chuksorji.nycschool.utils

import java.io.IOException
import kotlin.random.Random

//Network Exception that should be thrown if we don't have any connection, which is our first line of defense
class NoNetworkException : IOException()

//Random images for displaying since there's no thumbnails associated.
fun getPicture(): String {
    val result = mutableListOf(
        "https://metro.co.uk/wp-content/uploads/2016/02/gettyimages-169271477.jpg?quality=90&strip=all&zoom=1&resize=644%2C439",
        "https://previews.123rf.com/images/urfingus/urfingus1602/urfingus160200343/53058397-3d-illustration-of-modern-minimalistic-library-for-otdha.jpg",
        "https://i.pinimg.com/originals/78/e0/da/78e0da8a070411094da1b150175b308e.jpg",
        "https://wp.rockfordconstruction.com/wp-content/uploads/2019/01/Potters_House_Schools_02.jpg",
        "https://wp.rockfordconstruction.com/wp-content/uploads/2019/01/Potters_House_Schools_05.jpg",
        "https://bartongroupllc.com/wp-content/uploads/Patio-3.jpg",
        "https://hips.hearstapps.com/edc.h-cdn.co/assets/16/51/lichten-craig.jpg",
        "https://static3.depositphotos.com/1008025/237/i/600/depositphotos_2371440-stock-photo-modern-school-building.jpg",
        "http://schoolconstructionnews.com/wp-content/uploads/2020/06/FloridaHighSchool-800x445.jpg",
        "https://st2.depositphotos.com/2673929/8636/i/950/depositphotos_86365348-stock-photo-modern-workplaces-in-a-modern.jpg"
    )
    return result[Random.nextInt(0, 8)]
}

