package com.chuksorji.nycschool

import android.app.Application
import androidx.databinding.library.BuildConfig
import com.chuksorji.nycschool.di.PrimaryModules.databaseModule
import com.chuksorji.nycschool.di.PrimaryModules.repositoryModule
import com.chuksorji.nycschool.di.PrimaryModules.viewModelModule
import com.chuksorji.nycschool.di.NetworkModule.RetrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class SchoolApp : Application() {
    override fun onCreate() {
        super.onCreate()
        setUpTimber()

        //Koin is initialized at this point, we have all our modules ready to be instantiated at RunTime
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@SchoolApp)
            modules(
                listOf(
                    RetrofitModule,
                    viewModelModule,
                    databaseModule,
                    repositoryModule
                )
            )
        }
    }

    //Setting up our Timber for debugging purposes
    private fun setUpTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}