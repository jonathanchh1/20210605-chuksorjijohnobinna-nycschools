package com.chuksorji.nycschool.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
/*
  We have two entities here for each of our end-point calls.
 */
@Entity(tableName = "table_school")
@Parcelize
data class School constructor(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @SerializedName("dbn")
    @ColumnInfo(name = "dbn") var dbn: String? = null,
    @SerializedName("school_name")
    @ColumnInfo(name = "school_name") var school_name: String? = null,
    @SerializedName("neighborhood")
    @ColumnInfo(name = "neighborhood") var neighborhood: String? = null,
    @SerializedName("extracurricular_activities")
    @ColumnInfo(name = "extracurricular_activities") var ExtraCurri: String? = null,
    @SerializedName("phone_number")
    @ColumnInfo(name = "phone_number") var PhoneNumber: String? = null,
    @SerializedName("subway")
    @ColumnInfo(name = "subway") var subway: String? = null,
    @SerializedName("location")
    @ColumnInfo(name = "location") var location: String? = null,
    @SerializedName("grades2018")
    @ColumnInfo(name = "grades2018") var grades: String? = null,
    @SerializedName("primary_address_line_1")
    @ColumnInfo(name = "primary_address_line_1") var primaryAddress: String? = null,
    @SerializedName("overview_paragraph")
    @ColumnInfo(name = "overview_paragraph") var description: String? = null,
    @SerializedName("bus")
    @ColumnInfo(name = "bus") var bus: String? = null,
    @SerializedName("academicopportunities1")
    @ColumnInfo(name = "academicopportunities1") var academicopportunities1: String? = null,
    @SerializedName("academicopportunities")
    @ColumnInfo(name = "academicopportunities2") var academicOpportunities: String? = null,
    @SerializedName("school_sports")
    @ColumnInfo(name = "school_sports") var school_sports: String? = null,
    @SerializedName("total_students")
    @ColumnInfo(name = "total_students") var total_students: String? = null,
    @SerializedName("requirement1_1")
    @ColumnInfo(name = "requirement1_1") var requirement1_1: String? = null,
    @SerializedName("program1")
    @ColumnInfo(name = "program1") var program: String? = null,
    @SerializedName("website")
    @ColumnInfo(name = "website") var webUrl: String? = null,
) : Parcelable

@Entity(tableName = "tables_sat")
@Parcelize
data class SatDetail constructor(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @SerializedName("dbn")
    @ColumnInfo(name = "dbn") var dbn: String? = null,
    @SerializedName("school_name")
    @ColumnInfo(name = "school_name") var school_title: String? = null,

    @SerializedName("num_of_sat_test_takers")
    @ColumnInfo(name = "num_of_sat_test_takers") var num_takers: String? = null,

    @SerializedName("sat_math_avg_score")
    @ColumnInfo(name = "sat_math_avg_score") var math_avg: String? = null,

    @SerializedName("sat_writing_avg_score")
    @ColumnInfo(name = "sat_writing_avg_score") var writing_avg: String? = null,

    @SerializedName("sat_critical_reading_avg_score")
    @ColumnInfo(name = "sat_critical_reading_avg_score") var reading_avg: String? = null, ) : Parcelable


