package com.chuksorji.nycschool.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.chuksorji.nycschool.R
import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [School::class, SatDetail::class,], version = 4, exportSchema = false)
abstract class SchoolRoomDatabase : RoomDatabase() {
    abstract fun posterDao(): SchoolDao
    abstract fun satDao(): SatDao
    //we handle all db instantiation at this point with all our entities
    companion object {
        @Volatile
        private var dbInstance: SchoolRoomDatabase? = null
        fun getDatabase(context: Context, scope: CoroutineScope): SchoolRoomDatabase {
            return dbInstance ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    SchoolRoomDatabase::class.java,
                    context.getString(R.string.database_name)
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(SchoolRoomDatabaseCallback(scope))
                    .build()

                dbInstance = instance
                instance
            }
        }

        class SchoolRoomDatabaseCallback(var scope: CoroutineScope) : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                dbInstance?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.posterDao())
                    }
                }
            }
        }

        //we will delete all locally held data from our local db whenever we completely close out the application here,
        private fun populateDatabase(newsDao: SchoolDao) {
            newsDao.deleteAll()
        }
    }

}