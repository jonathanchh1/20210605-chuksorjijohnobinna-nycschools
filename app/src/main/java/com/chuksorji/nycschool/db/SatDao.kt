package com.chuksorji.nycschool.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chuksorji.nycschool.models.SatDetail
//our sat dao set up, which would allow us to store all sat data.
@Dao
interface SatDao {
    @Query("SELECT * FROM tables_sat ORDER BY id DESC")
    fun getAvailableSats(): LiveData<MutableList<SatDetail>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSat(posters: MutableList<SatDetail>)
    @Query("SELECT * FROM tables_sat where dbn=:dbn")
    fun getSatById(dbn : String) : SatDetail
    @Query("DELETE FROM tables_sat")
    fun deleteAll()
}