package com.chuksorji.nycschool.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School

//our school dao set up, which would allow us to store all school data.
@Dao
interface SchoolDao {
    @Query("SELECT * FROM table_school ORDER BY id DESC")
    fun getAvailableSchool(): LiveData<MutableList<School>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchool(posters: MutableList<School>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSat(posters: MutableList<SatDetail>)

    @Query("SELECT * FROM table_school where dbn=:dbn")
    fun getSatById(dbn : String) : SatDetail

    @Query("DELETE FROM table_school")
    fun deleteAll()
}