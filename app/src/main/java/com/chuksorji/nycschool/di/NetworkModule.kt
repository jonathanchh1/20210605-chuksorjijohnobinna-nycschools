package com.chuksorji.nycschool.di

import com.chuksorji.nycschool.BuildConfig
import com.chuksorji.nycschool.network.ConnectivityInterceptorImpl
import com.chuksorji.nycschool.network.NetworkFinder
import com.chuksorji.nycschool.network.NetworkServices
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.scope.Scope
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

//Network Module that allows for endpoint call.
object NetworkModule {
    private const val CONNECT_TIMEOUT = 15L
    private const val WRITE_TIMEOUT = 15L
    private const val READ_TIMEOUT = 15L
    private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

    private fun gsonConverter(): Gson = GsonBuilder()
        .setLenient()
        .create()

    val RetrofitModule = module {
        single { Cache(androidApplication().cacheDir, 10L * 1024 * 1024) }
        single { gsonConverter() }
        single { retrofitHttpClient() }
        single { retrofitBuilder() }
        single { networkServicesFor(get()) }
        single {
            Interceptor { chain ->
                chain.proceed(chain.request().newBuilder().apply {
                    header("Accept", "application/json")
                }.build())
            }
        }
        single { NetworkFinder(get()) }
    }

    private fun Scope.retrofitBuilder(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(get())
            .build()
    }

    fun networkServicesFor(retrofit: Retrofit): NetworkServices {
        return retrofit.create(NetworkServices::class.java)
    }

    private fun Scope.retrofitHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            cache(get())
            connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            retryOnConnectionFailure(true)
                .addInterceptor(ConnectivityInterceptorImpl(androidContext()))
            addInterceptor(interceptor = get())
            addInterceptor(HttpLoggingInterceptor().apply {
                //checking our logs here to see whether we have something...
                level = if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor.Level.HEADERS
                } else {
                    HttpLoggingInterceptor.Level.NONE
                }
            })
        }.build()
    }
}

