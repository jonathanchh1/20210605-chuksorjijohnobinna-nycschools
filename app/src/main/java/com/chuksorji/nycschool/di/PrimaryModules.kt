package com.chuksorji.nycschool.di

import com.chuksorji.nycschool.db.SchoolRoomDatabase
import com.chuksorji.nycschool.repo.DataManagerImpl
import com.chuksorji.nycschool.repo.DataPersistenceImpl
import com.chuksorji.nycschool.repo.NetworkDataSourceImpl
import com.chuksorji.nycschool.ui.main.MainViewModelImpl
import com.chuksorji.nycschool.ui.main.PresenterViewModelImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module



object PrimaryModules {
    //All our view models are instantiated here.
    val viewModelModule = module {
        viewModel { MainViewModelImpl() }
        viewModel { PresenterViewModelImpl(get()) }
    }

    //All instances of our daos is provided at this point.
    val databaseModule = module {
        single { SchoolRoomDatabase.getDatabase(androidContext(), CoroutineScope(Dispatchers.IO)).posterDao() }
        single { SchoolRoomDatabase.getDatabase(androidContext(), CoroutineScope(Dispatchers.IO)).satDao() }
    }

    //All our business logic classes modules are handled here.
    val repositoryModule = module {
        single { NetworkDataSourceImpl(get()) }
        single { DataPersistenceImpl(get(), get()) }
        single { DataManagerImpl(get(), get(), CoroutineScope(Dispatchers.IO)) }
    }
}