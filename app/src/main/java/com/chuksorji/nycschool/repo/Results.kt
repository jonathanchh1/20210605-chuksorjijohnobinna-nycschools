package com.chuksorji.nycschool.repo

import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School


//Method is being used for Output from our Retrofit.
sealed class Output<out T : Any>{
    data class Success<out T : Any>(val output : T) : Output<T>()
    data class Error(val exception: Exception, val state: String?)  : Output<Nothing>()
}

//Response data model being used for response retrieval.
data class Response(
    var error: Any? = null,
    var schoolList: MutableList<School>? = mutableListOf(),
    var satDetail: MutableList<SatDetail>? = mutableListOf(),
    var state: String?
)

//Used to check what our response type looks like.
object ResultType {
    const val success = "Success"
    const val error = "error"
    const val slowConnection = "slowConnection"
    const val noConnection = "noConnection"
    const val internalError = "internalError"
}