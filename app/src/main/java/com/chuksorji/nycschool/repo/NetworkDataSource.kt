package com.chuksorji.nycschool.repo


interface NetworkDataSource {
    suspend fun getLatestSchool() : Response?
    suspend fun getLatestSats() : Response?

}