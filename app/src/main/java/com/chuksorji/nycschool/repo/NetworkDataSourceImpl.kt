package com.chuksorji.nycschool.repo

import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School
import com.chuksorji.nycschool.network.NetworkFinder
import com.chuksorji.nycschool.utils.NoNetworkException
import timber.log.Timber

class NetworkDataSourceImpl(private val networkFinder: NetworkFinder) : NetworkDataSource, BaseRepository() {

    /*
      @Todo because of time constraint, I decided to handle both instances of school and sat in two different methods.
         Ideally, I would make it generic enough to utilize either one method for both network calls or one list object as oppose
         to schoolList and satList.
     */

    //Both methods in this class does the same thing, we are doing our network call here and handling whatever case arise.
    override suspend fun getLatestSchool(): Response? {
        try {
            var errorMessage: Any? = null
            var currentState: String? = null
            val latestPosterResponse = safeApiCall(
                call = {
                    networkFinder.getRetrievedSchools()
                },
                errorMessage = "Error Fetching School.. Something went wrong due to",
                { error, state ->
                    errorMessage = error
                    currentState = state
                }
            )
            return evaluateResponse(
                schoolData = latestPosterResponse,
                errorMessage = errorMessage,
                currentState = currentState
            )

        } catch (e: NoNetworkException) {
            Timber.e("No internet connection $e")
            return Response(
                "No internet connection",
                state = ResultType.noConnection
            )
        }
    }

    override suspend fun getLatestSats(): Response? {
        try {
            var errorMessage: Any? = null
            var currentState: String? = null
            val latestPosterResponse = safeApiCall(
                call = {
                    networkFinder.getRetrievedSat()
                },
                errorMessage = "Error Fetching Sat.. Something went wrong due to",
                { error, state ->
                    errorMessage = error
                    currentState = state
                })
            return evaluateResponse(
                satData = latestPosterResponse,
                errorMessage = errorMessage,
                currentState = currentState
            )
        } catch (e: NoNetworkException) {
            Timber.e("No internet connection $e")
            return Response(
                "No internet connection",
                state = ResultType.noConnection
            )
        }
    }


    //This method helps use evaluate whether we have a case of slow connection or error response and if it's not neither of those cases, then
    //we'll return either schoolList or satList.
    private fun evaluateResponse(
        schoolData: MutableList<School>? = mutableListOf(),
        satData: MutableList<SatDetail>? = mutableListOf(),
        errorMessage: Any?,
        currentState: String?
    ): Response {
        if (errorMessage != null && currentState != null) {
            return when (currentState) {
                ResultType.slowConnection -> {
                    Timber.e("Slow Internet Connection $errorMessage")
                    Response(
                        "Slow Internet Connection",
                        state = ResultType.slowConnection
                    )
                }
                else -> {
                    return Response(
                        error = errorMessage,
                        state = currentState
                    )
                }
            }
        }

        return if (schoolData?.count() ?: 0 > 0) {
            Response(schoolList = schoolData, state = ResultType.success)
        } else {
            Response(satDetail = satData, state = ResultType.success)
        }
    }


}