package com.chuksorji.nycschool.repo

import androidx.lifecycle.LiveData
import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School

//All methods for data Persistence is declared here.
interface DataPersistence {
    suspend fun insertSchool(source: MutableList<School>)
    suspend fun insertSat(source: MutableList<SatDetail>)
   suspend fun getSatById(dbn : String) : SatDetail
    fun getAvailableSchools(): LiveData<MutableList<School>>
    fun getAvailableSats(): LiveData<MutableList<SatDetail>>
    suspend fun deleteAllRecords()
}