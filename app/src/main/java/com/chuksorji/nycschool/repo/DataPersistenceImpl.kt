package com.chuksorji.nycschool.repo

import androidx.lifecycle.LiveData
import com.chuksorji.nycschool.db.SchoolDao
import com.chuksorji.nycschool.db.SatDao
import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


//All data persistence access happens here, which clearly defines our entry point for accessing dao or storing data
class DataPersistenceImpl constructor(private var postDao: SchoolDao, private var satDao : SatDao) : DataPersistence {
    override suspend fun insertSchool(source: MutableList<School>) {
        return withContext(Dispatchers.IO) {
            return@withContext postDao.insertSchool(source)
        }
    }

    override suspend fun insertSat(source: MutableList<SatDetail>) {
        return withContext(Dispatchers.IO) {
            return@withContext satDao.insertSat(source)
        }
    }

    override suspend fun getSatById(dbn: String): SatDetail {
        return satDao.getSatById(dbn)
    }

    override fun getAvailableSchools(): LiveData<MutableList<School>> {
        return postDao.getAvailableSchool()
    }

    override fun getAvailableSats(): LiveData<MutableList<SatDetail>> {
        return satDao.getAvailableSats()
    }

    override suspend fun deleteAllRecords() {
        return withContext(Dispatchers.IO) {
            return@withContext postDao.deleteAll()
        }
    }
}