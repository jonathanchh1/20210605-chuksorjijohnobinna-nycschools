package com.chuksorji.nycschool.repo

import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

/*
  Our Generic endpoint caller that evaluates endpoint response and allows use to easily consume our data-set and informs us of any issues
 */
open class BaseRepository {
    suspend fun <T : Any> safeApiCall(
        call: suspend () -> Response<T>,
        errorMessage: String,
        throwable: (error: (Any), state: (String?)) -> Unit = { _, _ -> }
    ): T? {
        val result = apiOutput(call, errorMessage)
        var output: T? = null
        when (result) {
            is Output.Success -> output = result.output
            is Output.Error -> {
                throwable.invoke("${result.exception}", result.state)
            }
        }
        return output
    }


    /*
      This method handles several cases of http exception that we might receive if our network call fails
      and returns response body if our call was successful
     */
    private suspend fun <T : Any> apiOutput(
        call: suspend () -> Response<T>,
        error: String,
    ): Output<T> {
        val response = call.invoke()
        return if (response.isSuccessful)
            Output.Success(response.body()!!)
        else
            when (response.code()) {
                ResponseCodeType.timeout, ResponseCodeType.slowConnection -> {
                    Output.Error(
                        SocketTimeoutException("${response.message()} $error"),
                        ResultType.slowConnection
                    )
                }
                ResponseCodeType.internalError, ResponseCodeType.notFound -> {
                    Output.Error(
                        InterruptedException("${response.message()} $error"),
                        ResultType.internalError
                    )
                }
                else -> Output.Error(
                    IOException("${response.message()} $error"),
                    ResultType.noConnection
                )
            }

    }

    private object ResponseCodeType {
        const val timeout = 408
        const val notFound = 404
        const val slowConnection = 503
        const val internalError = 500
    }
}
