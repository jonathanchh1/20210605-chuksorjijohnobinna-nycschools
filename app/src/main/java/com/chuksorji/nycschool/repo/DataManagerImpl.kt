package com.chuksorji.nycschool.repo

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class DataManagerImpl constructor(
    private var dataPersist: DataPersistenceImpl,
    private var dataSource: NetworkDataSourceImpl,
    private var scope: CoroutineScope,
) : DataManager {
    /*
       This method is utilized for calling and retrieving data from server
     */
    override fun persistNetworkDataWith(callback: suspend (error: Any?, state: String?) -> Unit) {
        scope.launch(Dispatchers.IO) {
            val school = dataSource.getLatestSchool()
            val sat = dataSource.getLatestSats()
            school?.fetchResult(callback)
            sat?.fetchResult(callback)
        }
    }

    /*
      Once data is received, we'll run this method to store it in our DB for offline services and process any errors.
     */
    override suspend fun Response.fetchResult(
        callback: suspend (error: Any?, state: String?) -> Unit
    ) {
        val result = this
        when (result.state) {
            ResultType.success -> {
                dataPersist.apply {
                    try {
                        val schools = result.schoolList ?: mutableListOf()
                        val sats = result.satDetail ?: mutableListOf()
                        if (schools.count() > 0) {
                            insertSchool(schools)
                        }

                        if (sats.count() > 0) {
                            insertSat(sats)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Timber.e("$e.message")
                    }
                }
            }
            ResultType.error -> {
                callback.invoke(result.error, result.state)
            }
            ResultType.slowConnection -> {
                callback.invoke(result.error, result.state)
            }
            ResultType.noConnection -> {
                callback.invoke(result.error, result.state)
            }
        }
    }

    /*
       calling our data-persistence to check whether we have sat object
     */
    override suspend fun findSatById(dbn: String) = dataPersist.getSatById(dbn)

    /*
     All school data-models will be returned using this
     */
    override fun getAllDBData() = dataPersist.getAvailableSchools()
}