package com.chuksorji.nycschool.repo

import androidx.lifecycle.LiveData
import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School

/*
  Following principle of Interface Segregation, we're going to make sure there's unique interface for each of our classes, allowing flexibility to either remove a method or add one
 */
interface DataManager {
    fun persistNetworkDataWith(callback: suspend (error: Any?, state: String?) -> Unit)
    suspend fun Response.fetchResult(callback:suspend (error: Any?, state: String?) -> Unit)
    fun getAllDBData(): LiveData<MutableList<School>>
    suspend fun findSatById(dbn: String): SatDetail
}