package com.chuksorji.nycschool.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.chuksorji.nycschool.R
import com.chuksorji.nycschool.databinding.SchoolContentBinding
import com.chuksorji.nycschool.models.School
import com.chuksorji.nycschool.ui.main.PresenterViewModelImpl

/*
  Just a basic implementation of ListAdapter with DiffUtil to handle any changes to data-set without
  having to continuously call NotifyDataSetChanged()
 */
class SchoolAdapter : ListAdapter<School, SchoolAdapter.CarouselViewHolder>(CarouselDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarouselViewHolder {
        val binding: SchoolContentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.school_content, parent, false
        )
        return CarouselViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CarouselViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun getItemViewType(position: Int) = position

    fun updateAdapter(items: MutableList<School>) {
        this.submitList(items)
    }


    inner class CarouselViewHolder(val binding: SchoolContentBinding) : RecyclerView.ViewHolder(binding.root) {
        //This method is used to pass our current School model to our ViewModel and any Selected Data-Model will be Triggered here
        fun bind(item: School) {
            if (binding.posterItem == null) {
                binding.posterItem = PresenterViewModelImpl(item)
            } else {
                binding.posterItem?.source = item
            }
        }

    }

    companion object CarouselDiffUtil : DiffUtil.ItemCallback<School>() {
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.school_name == newItem.school_name && oldItem.description == newItem.description
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.neighborhood == newItem.neighborhood && oldItem.dbn == newItem.dbn
        }
    }

}