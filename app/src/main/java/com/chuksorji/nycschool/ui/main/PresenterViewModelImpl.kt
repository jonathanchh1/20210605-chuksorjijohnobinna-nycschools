package com.chuksorji.nycschool.ui.main

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.chuksorji.nycschool.R
import com.chuksorji.nycschool.databinding.ArticleViewContentBinding
import com.chuksorji.nycschool.models.SatDetail
import com.chuksorji.nycschool.models.School
import com.chuksorji.nycschool.repo.DataManagerImpl
import com.chuksorji.nycschool.utils.getPicture
import kotlinx.android.synthetic.main.article_view_content.*
import kotlinx.coroutines.*
import org.koin.java.KoinJavaComponent


class PresenterViewModelImpl(val source: School) : ViewModel(), PresenterViewModel {
    private val dataManager: DataManagerImpl by KoinJavaComponent.inject(DataManagerImpl::class.java)
    /*
      All variables being used to display our data.
     */
    private val isOpen = ObservableField(false)
    override val author: String get() = source.primaryAddress.checkContentAvailability()
    override val desc: String get() = source.description.checkContentAvailability()
    override val title: String get() = source.school_name.checkContentAvailability()
    override val url: String get() = source.webUrl ?: ""
    override val location: String get() = source.location.checkContentAvailability()
    override val overview: String get() = source.ExtraCurri.checkContentAvailability()
    override val phoneNumber: String get() = source.PhoneNumber.checkContentAvailability()
    override val primary_address: String get() = source.primaryAddress.checkContentAvailability()
    override val subway: String get() = source.subway.checkContentAvailability()
    override val bus: String get() = source.bus.checkContentAvailability()
    override val academic get() = source.academicopportunities1.checkContentAvailability()
    private var satDetail: SatDetail? = null
    override val school_sport: String get() = source.school_sports.checkContentAvailability()
    override val totalStudent: String get() = source.total_students.checkContentAvailability()
    override val grades: String get() = source.grades.checkContentAvailability()

    override val detailThumbnail: String get() = getPicture()
    override val thumbnail: String get() = getPicture()

    override fun onClick(view: View) {
        //Whenever we launch our dialog-box, we'll block the thread before making our DB call to retrieve our SatDetail Object if it exists.
        satDetail = runBlocking {
            withContext(Dispatchers.IO) {
                dataManager.findSatById(
                    source.dbn ?: ""
                )
            }
        }
        loadInfoModal(view)
    }


    //All variables that are being used to display sat-details.
    override val detailTitle: String get() = satDetail?.school_title.checkContentAvailability()
    override val mathResult: String get() = satDetail?.math_avg.checkContentAvailability()
    override val readResult: String get() = satDetail?.reading_avg.checkContentAvailability()
    override val writeResult: String get() = satDetail?.writing_avg.checkContentAvailability()


    /*
     This method allows us to check whether have data
      and if we don't we can display a message to user to let them know, there's no data availability
     */
    private fun String?.checkContentAvailability(): String {
        return if (this != "s") this ?: "Content is Unavailable" else "Content is Unavailable"
    }

    //Once a user clicks on continue, this method is called.
    override fun onReadMore(view: View) {
        val intent = Intent(Intent.ACTION_VIEW)
        val httpUrl = "http://"
        intent.data = Uri.parse("${httpUrl}$url")
        view.context.startActivity(intent)
    }

    /*
       This method is being used to show a dialog-box,
       I decided to display the detail-screen with this because I thought it would be more efficient and I didn't see why an Activity was required
     */
    override fun loadInfoModal(view : View) {
        //This logic allows for prevention of showing two instances of the dialog box, so, we first check whether its open and if its already open, we'll return here.
        if (isOpen.get() == true) return

        //At this point we'll set up our styles which is declared in themes
        val dialog = Dialog(view.context, R.style.LegendOverlayStyles)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.attributes?.windowAnimations = R.style.DialogTheme

        //Data-Binding is being used here in order to reuse this view-model and offer clean practice of re-usability.
        val binding: ArticleViewContentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(view.context),
            R.layout.article_view_content,
            null,
            false
        )
        binding.dialogView = this
        binding.executePendingBindings()
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawableResource(R.color.background_base)


        if (dialog.isShowing) {
            dialog.dismiss()
        } else {
            isOpen.set(true)
            dialog.show()
        }

        //We'll dismiss our dialog-box and return false for our isOpen if user press exit icon
        dialog.leave_button?.setOnClickListener {
            isOpen.set(false)
            dialog.dismiss()
        }
    }


    /*
       This method is being used for sharing the school's website and school-name.
     */
    override fun onSharedClicked(view: View) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, source.school_name)
        intent.putExtra(Intent.EXTRA_TEXT, source.webUrl)
        view.context.startActivity(Intent.createChooser(intent, "Shared Options"))
    }
}