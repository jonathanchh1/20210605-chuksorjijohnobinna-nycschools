package com.chuksorji.nycschool.ui.main


//All methods that are being used in MainViewModelImpl is here.
interface MainViewModel {
    fun fetchDataForUi()
    fun requestErrorForUI(state: String, error : Any?): String?
    fun onRefresh()
    fun initializeLoader()
}