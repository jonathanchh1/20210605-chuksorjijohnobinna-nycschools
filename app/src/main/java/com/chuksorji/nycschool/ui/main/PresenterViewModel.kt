package com.chuksorji.nycschool.ui.main

import android.view.View

//All methods that are being used in PresenterViewModelImpl is here.
interface PresenterViewModel {
    val thumbnail: String
    val desc: String
    val title: String
    val author: String
    val url: String
    val overview: String?
    val mathResult: String
    val writeResult: String
    val readResult: String
    val detailTitle: String
    val phoneNumber: String
    val subway: String
    val location: String
    val primary_address: String
    val school_sport: String
    val bus: String
    val academic: String
    val detailThumbnail : String
    val grades: String
    val totalStudent: String
    fun onClick(view: View)
    fun onReadMore(view: View)
    fun onSharedClicked(view: View)
    fun loadInfoModal(view: View)
}
