package com.chuksorji.nycschool.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chuksorji.nycschool.R
import com.chuksorji.nycschool.adapter.SchoolAdapter
import com.chuksorji.nycschool.databinding.MainFragmentBinding
import com.chuksorji.nycschool.repo.ResultType
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val mainViewModel by viewModel<MainViewModelImpl>()
    private var binding: MainFragmentBinding? = null
    private var schoolAdapter: SchoolAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.main_fragment,
            container,
            false
        )
        binding?.mainVModel = mainViewModel
        binding?.lifecycleOwner = this
        binding?.executePendingBindings()
        schoolAdapter = SchoolAdapter()
        binding?.schoolView?.adapter = schoolAdapter
        binding?.schoolView?.layoutManager = GridLayoutManager(context, 2)

        mainViewModel.loadFetchedData.observe(viewLifecycleOwner) { result ->
            /*
               Whenever we receive an update from our view-model, we'll evaluate whether
               there's data availability before deciding on what to display. This principle of defensive
               coding allows us to avoid unpleasant experience for the user.
             */
            when (result.state) {
                ResultType.success -> {
                    binding?.schoolView.validateDataAvailability()
                    schoolAdapter?.updateAdapter(result.schoolList ?: mutableListOf())
                }

                ResultType.error, ResultType.noConnection, ResultType.slowConnection -> {
                    binding?.schoolView?.visibility = View.GONE
                    binding?.errorContainer?.visibility = View.VISIBLE
                    binding?.message?.text = "${result.error}"

                }
            }
        }
        return binding!!.root
    }

    /*
      This method is being used to evaluate whether we lost visibility of recyclerView
      when we clicked on retry again and if it's not visible, we'll make it visible again and
      remove errorContainer visibility.
     */

    private fun RecyclerView?.validateDataAvailability() {
        if (this?.visibility == View.GONE) {
            this.visibility = View.VISIBLE
        }
        binding?.errorContainer?.visibility = View.GONE
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}