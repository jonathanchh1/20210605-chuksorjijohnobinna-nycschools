package com.chuksorji.nycschool.ui.main

import androidx.lifecycle.*
import com.chuksorji.nycschool.repo.DataManagerImpl
import com.chuksorji.nycschool.repo.Response
import com.chuksorji.nycschool.repo.ResultType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject

class MainViewModelImpl : ViewModel(), MainViewModel {
    //we'll retrieve our dataManager instance at this point in order to call our end-point to fetch data
    private val dataManager: DataManagerImpl by inject(DataManagerImpl::class.java)

    /*
      we are gonna use live-data to make sure we are asynchronously reacting to state of our data and informing ui whenever there's a change.
     */
    private val _dataSource = MutableLiveData<Response>()
    val loadFetchedData: LiveData<Response> get() = _dataSource
    private val _isLoadingData = MutableLiveData(true)
    val isLoadingData: LiveData<Boolean>
        get() = _isLoadingData


    init {
        /*
          First we initialize our data once our view-model has been created here.
         */
        initializeLoader()
    }

    override fun initializeLoader() {

        viewModelScope.launch {
            /*
              After calling our end-point, we'll retrieve any changes to data-set from business layer.
             */
            dataManager.persistNetworkDataWith { errorText, state ->
                if (errorText != null) {
                    _dataSource.postValue(
                        /*
                         We are returning a response object at this point because maybe we want to modify the error message from our business-layer in order to better inform the user
                         as to what action that is required to be taken.
                         */
                        Response(
                            error = requestErrorForUI(
                                state ?: "",
                                errorText
                            ),
                            state = state
                        )
                    )
                    _isLoadingData.postValue(false)
                }
            }
        }
        fetchDataForUi()
    }

    /*
     This method is used to add description to our error message and can be modified however we desire, either retrieve the error text from server or hard-code it.
     */
    override fun requestErrorForUI(state: String, error: Any?): String? {
        return when (state) {
            ResultType.noConnection -> {
                return "${error}, please reconnect and try again."
            }

            ResultType.slowConnection -> {
                return "${error}, please retry again once your connection improves."
            }

            ResultType.internalError -> {
                return "${error}, please retry again and if you still can't connect, retry in 5 minutes."
            }
            else -> null
        }
    }

    /*
      At this point, after fetching our data-set, we'll convert it to flow to easily fetch it whenever there's a change and inform our live-data and remove our progress-bar
     */
    override fun fetchDataForUi() {
        viewModelScope.launch(Dispatchers.Main) {
            dataManager.getAllDBData().asFlow().collect { result ->
                if (result.count() > 0) {
                    _dataSource.postValue(Response(schoolList = result, state = ResultType.success))
                    _isLoadingData.postValue(false)
                }
            }

        }

    }


    /*
      This method is used to achieve bi-directional data-flow from our ui to view-model and vice versa, if we have an api-failure and unable to retrieve our data,
      We'll call our end-point again using this method.

      //TODO this method is a duplicate of our initializeLoader()
         however for better readability i duplicated it,  i'll perhaps consolidate both later but for expediency its done in this way.
     */
    override fun onRefresh() {
        _isLoadingData.postValue(true)
        viewModelScope.launch {
            dataManager.persistNetworkDataWith { errorText, state ->
                if (errorText != null) {
                    _dataSource.postValue(
                        Response(
                            error = requestErrorForUI(
                                state ?: "",
                                errorText
                            ), state = state
                        )
                    )
                    /*
                A delay is being applied here because our end-point might return quickly whenever it's perceived that the data-set state hasn't changed,
               So, we would like our end-user to see the loader a bit longer prior to removing it.
           */
                    delay(900)
                    _isLoadingData.postValue(false)
                }
            }
        }
        fetchDataForUi()

    }
}